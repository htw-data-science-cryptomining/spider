package waybackcsv

import (
	"os"
	"bufio"
	"strings"
	"strconv"
	"io/ioutil"
	"encoding/json"
	"fmt"
	"../communication"
)

type WBReader struct {
	File string
	From uint64
	To uint64
	All bool
	num uint64
}

func NewReader(file string) *WBReader {
	return &WBReader{
		File: file,
		From: 0,
		To: 0,
		num: 0,
	}
}

func (wb *WBReader) ReadCfg(cfg string) {
	file, err := ioutil.ReadFile(cfg)

	if err != nil {
			panic("cant read cfg file")
	}

	jsonCfg := make(map[string]interface{})

	err = json.Unmarshal(file, &jsonCfg)

	if err != nil {
		panic("cant read json")
	}

	value := jsonCfg["from"].(string)
	from, _ := strconv.ParseUint(value, 10, 64)

	value = jsonCfg["len"].(string)
	to, _ := strconv.ParseUint(value, 10, 64)

	all := false

	if jsonCfg["all"].(string) == "true" {
		all = true
	}

	wb.From = from
	wb.To = from + to
	wb.All = all
}

func (wb *WBReader) WriteIn(channel chan<- interface{}) {
	file, err := os.Open(wb.File)

	if err != nil {
		panic("cant open csv file")
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	for wb.num < wb.From {
		scanner.Scan()
		wb.num++
	}

	for scanner.Scan() {
		if wb.All || wb.num < wb.To {
			line := scanner.Text()
			fields := strings.Split(line, ",")

			if len(fields) < 3 {
				continue
			}

			date, err := strconv.ParseUint(fields[1], 10, 64)

			if err != nil {
				continue
			}

			topaddress := fields[0]
			address := fields[2]

			dateId := ""
			dateToString := strconv.FormatUint(date, 10)
			addressFields := strings.Split(address, "/")

			for i := 0; i < len(addressFields); i++ {
				if strings.Contains(addressFields[i], dateToString) {
					dateId = addressFields[i]
					break
				}
			}

			address = strings.Replace(address, dateId, dateId + "id_", 1)

			record := communication.NewDataCluster(date, address, topaddress)

			channel <- record
			fmt.Printf("CSV:\tCluster - %d %s\n", date, address)
		} else {
			break
		}

		wb.num++
	}
}
