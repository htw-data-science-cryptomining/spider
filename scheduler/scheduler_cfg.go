package scheduler

import (
	"encoding/json"
	"strconv"
	"io/ioutil"
)

func (sched *Sched) ReadCfg(cfg string) {
	file, err := ioutil.ReadFile(cfg)

	if err != nil {
		panic("cant read cfg file")
	}

	jsonCfg := make(map[string]interface{})

	err = json.Unmarshal(file, &jsonCfg)

	if err != nil {
		panic("cant read json")
	}

	value := jsonCfg["tries"].(string)
	tries, _ := strconv.ParseUint(value, 10, 32)

	sched.tries = uint32(tries)
}
