package scheduler

/*

*/

import (
	"errors"
	"sync"
	"runtime"
	"time"
)

var (
	ErrSDead = errors.New("scheduler: killed in action")
	ErrDMissing = errors.New("scheduler: missing data")
	ErrWNotResponding = errors.New("scheduler: worker not responding")
	ErrWTimeout = errors.New("scheduler: worker timed out")
	ErrPFile = errors.New("scheduler: cant read file")
	ErrPValue = errors.New("scheduler: cant read value")
)

const (
	SigAbort = 0
	SigRestart = 1
	SigSave = 2
)

type Sched struct {
	C chan interface{}
	Receiving chan bool
	TimeStamp time.Time
	worker uint32
	wg sync.WaitGroup
	signal chan uint16
	flags uint16
	queue *Queue
	pending *Queue
	requests chan bool
	frames Frames
	tries uint32
}

type Frames struct {
	frame uint64
	lock sync.Mutex
}

func (sched *Sched) NextFrame() uint64 {
	sched.frames.lock.Lock()
	defer sched.frames.lock.Unlock()

	sched.frames.frame++
	return sched.frames.frame-1
}

func (sched *Sched) Worker(worker uint32) {
	sched.worker = worker
}

func (sched *Sched) Start() {
	runtime.GOMAXPROCS(runtime.NumCPU()*2)

	sched.C = make(chan interface{})
	sched.Receiving = make(chan bool)
	sched.TimeStamp = time.Now()
	sched.signal = make(chan uint16)
	sched.requests = make(chan bool, 1)
	sched.flags = 0
	sched.queue = NewQueue()
	sched.pending = NewQueue()
	sched.frames = Frames{
		frame: 0,
		lock: sync.Mutex{},
	}

	sched.wg.Add(1)
	go sched.run()
}

func (sched *Sched) Wait() {
	close(sched.C)
	sched.wg.Wait()
}

func (sched *Sched) Stop() {
	sched.SendSignal(0)
}

func (sched *Sched) SendSignal(sig uint16) {
	sched.signal <- sig
}

func (sched *Sched) HandleSignal(sig uint16) {
	if sched.HasSignalFlag(sig) {
		sched.ClearSignalFlag(sig)
	} else {
		sched.SetSignalFlag(sig)
	}
}

func (sched *Sched) SetSignalFlag(pos uint16) {
	sched.flags |= (1 << pos)
}

func (sched *Sched) ClearSignalFlag(pos uint16) {
	mask := ^(uint16(1) << pos)
	sched.flags &= mask
}

func (sched *Sched) HasSignalFlag(pos uint16) bool {
	flag := sched.flags & (1 << pos)
	return (flag > 0)
}

func (sched *Sched) run() {
	defer sched.wg.Done()

	sched.wg.Add(1)
	//go sched.performance()
	go sched.distribute()

	for !sched.HasSignalFlag(SigAbort) {
		sig := <- sched.signal
		sched.HandleSignal(sig)
	}
}
