package scheduler

import (
	"../communication"
)

func (sched *Sched) queueRequest(req interface{}, frame uint64) {
	sched.queue.Append(req)
	sched.pending.Append(frame)
	sched.requestAvailable();
}

func (sched *Sched) pushRequest(req interface{}, frame uint64) {
	sched.queue.Push(req)
	sched.pending.Append(frame)
	sched.requestAvailable();
}

func (sched *Sched) requeueRequest(req interface{}) {
	sched.queue.Push(req)
	sched.requestAvailable();
}

func (sched *Sched) requestAvailable() {
	select {
	case sched.requests <- true:
		break
	default:
		break
	}
}

func (sched *Sched) distribute() {
	defer sched.wg.Done()

	var buffer []communication.DataCluster
	noMoreContent := false
	close := false

	for !close {
		switch true {
		case sched.queue.Size() == 0 && sched.pending.Size() < sched.worker && !noMoreContent:
			input, open := <- sched.C

			if !open {
				if len(buffer) > 0 {
					data := make([]communication.DataCluster, len(buffer))
					copy(data, buffer)
					request := communication.NewRequest(data, 0, sched.NextFrame())
					sched.queueRequest(request, request.Frame)

					buffer = buffer[:0]
				}

				noMoreContent = true
				sched.C = nil
				break
			}

			cluster := input.(communication.DataCluster)

			if len(buffer) == 0 {
				buffer = append(buffer, cluster)
			} else {
				if buffer[0].TopAddress != cluster.TopAddress {
					data := make([]communication.DataCluster, len(buffer))
					copy(data, buffer)
					request := communication.NewRequest(data, 0, sched.NextFrame())
					sched.queueRequest(request, request.Frame)

					buffer = buffer[:0]
				}

				buffer = append(buffer, cluster)
			}
		case sched.queue.Size() == 0 && sched.pending.Size() == 0 && noMoreContent:
			close = true
		default:
			select {
			case input := <- communication.ResponseChannel:
				input.ProcessError()
				copyFailed, copyAdd := input.CopyNeeded()

				if copyFailed && input.Try < sched.tries {
					request := communication.CopyFailedFromResponse(input)
					sched.requeueRequest(request)
				} else {
					sched.pending.Remove(input.Frame)
				}

				if copyAdd {
					request := communication.CopyAddFromResponse(input, sched.NextFrame())
					sched.pushRequest(request, request.Frame)
				}

				case <- sched.requests:
					cancel := false

					for !cancel {
						entry := sched.queue.First()

						if entry == nil {
							break
						}

						request := entry.Value.(communication.Request)

						select {
						case communication.RequestChannel <- request:
							sched.queue.Dequeue()
						default:
							cancel = true
							sched.requestAvailable();
						}
					}
			}
		}
	}

	sched.SendSignal(SigAbort)
}
