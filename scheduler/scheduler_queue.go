package scheduler

import (
	"sync"
	"container/list"
)

type Queue struct {
	list *list.List
	lock sync.Mutex
}

func NewQueue() *Queue {
	queue := &Queue{
		list.New(),
		sync.Mutex{},
	}
	return queue
}

func (queue *Queue) Push(value interface{}) {
	queue.lock.Lock()
	defer queue.lock.Unlock()

	queue.list.PushFront(value)
}

func (queue *Queue) Append(value interface{}) {
	queue.lock.Lock()
	defer queue.lock.Unlock()

	queue.list.PushBack(value)
}

func (queue *Queue) Remove(value interface{}) {
	queue.lock.Lock()
	defer queue.lock.Unlock()

	if value == nil {
		return
	}

	for entry := queue.First(); entry != nil; entry = entry.Next() {
		if entry.Value == value {
			queue.list.Remove(entry)
			break
		}
	}
}

func (queue *Queue) Dequeue() *list.Element {
	queue.lock.Lock()
	defer queue.lock.Unlock()

	entry := queue.First()

	if entry != nil {
		queue.list.Remove(entry)
	}

	return entry
}

func (queue *Queue) First() *list.Element {
  return queue.list.Front()
}

func (queue *Queue) Last() *list.Element {
  return queue.list.Back()
}

func (queue *Queue) Size() uint32 {
	return uint32(queue.list.Len())
}
