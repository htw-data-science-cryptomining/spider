package workerPool

import (
	"../worker"
	"../communication"
	"sync"
)

// attributes of a worker pool
type WorkerPool struct {
	reqChannel chan communication.Request
	resChannel chan communication.Response

	abortChannel chan struct{}
	activeWorkers int
	waitGroup sync.WaitGroup
}

// creates a new worker pool
func NewWorkerPool(reqChannel chan communication.Request, resChannel chan communication.Response) WorkerPool {
	return WorkerPool{
		reqChannel: reqChannel,
		resChannel: resChannel,
		abortChannel: make(chan struct{}),
		activeWorkers: 0,
	}
}

// resizes the amount of workers
func (w *WorkerPool) ResizeWorkers(delta int) {
	if delta > 0 {
		// create worker
		for i := 0; i < delta; i++ {
			newWorker := worker.NewWorker(w.reqChannel, w.resChannel, w.abortChannel, &w.waitGroup)
			w.waitGroup.Add(1)
			go newWorker.Run()
		}
		w.activeWorkers += delta
	} else if delta < 0 {
		// kill worker
		delta = -delta
		if delta > w.activeWorkers {
			delta = w.activeWorkers
		}
		for i := 0; i < delta; i++ {
			w.abortChannel <- struct{}{}
		}
		w.activeWorkers -= delta
	}
}

// waits until all active workers finished their task and stops them
func (w *WorkerPool) Close() {
	close(w.reqChannel)
	w.waitGroup.Wait()
}
