package main


import (
    "fmt"
    "./communication"
    "./scheduler"
    "./workerPool"
    "./waybackcsv"
)

func main() {
    communication.Establish()
    fmt.Printf("Communication established\n")

    sched := scheduler.Sched{}
    sched.ReadCfg("config.json")

    reader := waybackcsv.NewReader("waybackmachinedata.csv")
    reader.ReadCfg("config.json")

    pool := workerPool.NewWorkerPool(communication.RequestChannel, communication.ResponseChannel)
	fmt.Printf("Worker pool initialized")

    sched.Worker(50)
    sched.Start()
    fmt.Printf("Scheduler started (%s)\n", sched.TimeStamp.String())

    pool.ResizeWorkers(50)
    reader.WriteIn(sched.C)

    fmt.Printf("Wait for scheduler to complete\n")
    sched.Wait()
    fmt.Printf("Scheduling completed\n")
    pool.Close()
    fmt.Printf("Worker pool closed\n")
}
