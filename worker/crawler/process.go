package crawler

import (
	"regexp"
	"strings"
	"hash/fnv"
	"../../communication"
	"sync"
	"fmt"
	"time"
)

type BlacklistURL struct {
	Address string
	Date uint64
}

type Blacklist struct {
	urls map[BlacklistURL]uint32
	sum uint32
	lock sync.Mutex
}

var blacklist *Blacklist

func NewBlacklist() *Blacklist {
	return &Blacklist{
		urls: make(map[BlacklistURL]uint32),
		sum: 0,
		lock: sync.Mutex{},
	}
}

func (bl *Blacklist) Allowed(url string, date uint64) bool {
	bl.lock.Lock()
	defer bl.lock.Unlock()

	entry := BlacklistURL{
		Address: url,
		Date: date,
	}

	priority := bl.urls[entry]
	bl.sum++

	if priority == 0 {
		bl.urls[entry] = 1
		return true
	}

	bl.urls[entry]++
	return false
}

func (bl *Blacklist) Reduce() {
	bl.lock.Lock()
	defer bl.lock.Unlock()

	average := float32(bl.sum) / float32(len(bl.urls))

	for key, value := range bl.urls {
		if float32(value) < average {
			delete(bl.urls, key)
		}
	}
}

func init() {
	blacklist = NewBlacklist()

	go func() {
		for {
			<-time.After(60 * time.Second)
			blacklist.Reduce()
		}
	}()
}

func ExtractJs(webpage *Webpage) ([]communication.DataCluster, []string) {
	// create return types
	newLinks := make([]communication.DataCluster, 0)
	embeddedScripts := make([]string, 0)

	// check if website is javascript -> no need to parse
	if webpage.Ext == ".js" {
		return newLinks, embeddedScripts
	}

	// compile regex
	baseRegex := regexp.MustCompile(`(https?:\/\/.*?\/)`)
	scriptRegex := regexp.MustCompile(`<script.*?(?:src="(.+?)".*?)?(?:\/>|>([\s\S]*?)<\/script>)`)

	// find baseURL
	baseURL := baseRegex.FindStringSubmatch(webpage.Address)[1]

	// find scripts in site
	matches := scriptRegex.FindAllStringSubmatch(webpage.Content, -1)

	// iterate over cluster
	for _, match := range matches {
		var hash string
		// match has link?
		if match[1] != "" {
			if !strings.HasSuffix(baseURL, "/") {
				baseURL = baseURL+"/"
			}

			var completeURL string
			if strings.HasPrefix(match[1], "/") {
				completeURL = fmt.Sprintf("%sweb/%djs_%s", baseURL, webpage.Date, match[1])
			} else {
				completeURL = fmt.Sprintf("%sweb/%djs_/%s", baseURL, webpage.Date, match[1])
			}
			newLinks = append(newLinks, communication.NewDataCluster(webpage.Date, completeURL, webpage.TopAddress))
			hash = hashString(completeURL)
		} else {
			embeddedScripts = append(embeddedScripts, match[2])
			hash = hashString(match[2])
		}
		webpage.Content = strings.Replace(webpage.Content, match[0], `<script src="`+hash+`.js" />`, 1)
	}
	return newLinks, embeddedScripts
}

func hashString(value string) string {
	hash := fnv.New32a()
	hash.Write([]byte(value))
	return fmt.Sprintf("%d", hash.Sum32())
}
