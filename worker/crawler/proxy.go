package crawler

import (
	"encoding/json"
	"io/ioutil"
	"net/url"
	"strings"
	"math/rand"
	"time"
	"sync"
)

var (
	proxyList ProxyList
	proxyString string
)

type ProxyList struct {
	List []Proxy
	lock sync.Mutex
}

type Proxy struct {
	URL *url.URL
	Fails uint32
	Use uint32
	Index int
}

func init() {
	rand.Seed(time.Now().Unix())
	readProxyCfg()
	fields := strings.Split(proxyString, ",")
	proxyList = ProxyList{
		List: make([]Proxy, len(fields) + 1),
		lock: sync.Mutex{},
	}

	proxyList.lock.Lock()
	defer proxyList.lock.Unlock()

	for i, p := range fields {
		proxy, err := url.Parse(p)
		if err != nil {
			panic("Could not parse proxy: "+p)
		}
		proxyList.List[i] = Proxy{
				URL: proxy,
				Fails: 0,
				Use: 0,
				Index: i,
			}
	}

	proxyList.List[len(proxyList.List) - 1] = Proxy{
		URL: nil,
		Fails: 0,
		Use: 0,
		Index: len(proxyList.List) - 1,
	}
}

func readProxyCfg() {
	file, err := ioutil.ReadFile("config.json")
	if err != nil {
		panic("Cannot open crawler config file")
	}

	jsonCfg := make(map[string]interface{})

	err = json.Unmarshal(file, &jsonCfg)
	if err != nil {
		panic("Cannot read crawler config file")
	}

	proxyString = jsonCfg["proxy"].(string)
}

func (pl *ProxyList) selectNoProxy() *Proxy {
	proxyList.lock.Lock()
	defer proxyList.lock.Unlock()

	return &pl.List[len(pl.List) - 1]
}

func (pl *ProxyList) selectRandomProxy() *Proxy {
	proxyList.lock.Lock()
	defer proxyList.lock.Unlock()

	numA := rand.Intn(len(proxyList.List))
	numB := rand.Intn(len(proxyList.List))

	failA := float64(proxyList.List[numA].Fails) / float64(proxyList.List[numA].Use)
	failB := float64(proxyList.List[numB].Fails) / float64(proxyList.List[numB].Use)

	if failA < failB {
		proxyList.List[numA].Use = proxyList.List[numA].Use + 1
		return &proxyList.List[numA]
	}

	proxyList.List[numB].Use = proxyList.List[numB].Use + 1
	return &proxyList.List[numB]
}

func (pl *ProxyList) proxyFailed(proxy *Proxy) {
	proxyList.lock.Lock()
	defer proxyList.lock.Unlock()

	proxyList.List[proxy.Index].Fails = proxyList.List[proxy.Index].Fails + 1
}
