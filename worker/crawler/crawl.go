package crawler

import (
	"../../communication"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
	"fmt"
)

var (
	proxyArray []url.URL
)

type Webpage struct {
	TopAddress string
	Date       uint64
	Address    string
	Content    string
	Media      string
	Ext        string
}

func downloadedAlready(topAddress string, date uint64, address string) bool {
	hash := hashString(address)
	path := fmt.Sprintf("%s/%s/%d", SAVE_PATH, topAddress, date)
	listOfFiles, err := ioutil.ReadDir(path)
	if err != nil {
		return false
	}

	for _, file := range listOfFiles {
		if strings.HasPrefix(file.Name(), hash) {
			return true
		}
	}
	return false
}

func DownloadCluster(clusterArray []communication.DataCluster, client *http.Client, transport *http.Transport) ([]Webpage, []communication.DataCluster) {
	webpages := make([]Webpage, 0)
	clusterFailed := make([]communication.DataCluster, 0)

	for _, dataCluster := range clusterArray {
		if !blacklist.Allowed(dataCluster.Address, dataCluster.Date) {
			continue
		}

		// check if the website was downloaded already
		if downloadedAlready(dataCluster.TopAddress, dataCluster.Date, dataCluster.Address) {
			continue
		}
		webpage, media, err, proxy := getPage(&dataCluster, client, transport)

		if err != communication.ErrNone {
			dataCluster.Err = err
			fmt.Printf("Crawl Failed:\n%s, Error: %d Proxy: %s (%d/%d)\n", dataCluster.Address, dataCluster.Err, proxy.URL, proxy.Fails, proxy.Use)
			clusterFailed = append(clusterFailed, dataCluster)
		} else {
			fmt.Printf("Crawl Successful:\n%s, Proxy: %s (%d/%d)\n", dataCluster.Address, proxy.URL, proxy.Fails, proxy.Use)
			// add extension
			ext := ""
			switch {
			case strings.Contains(media, "html"):
				ext = ".html"
			case strings.Contains(media, "javascript"):
				ext = ".js"
			case strings.Contains(media, "php"):
				ext = ".php"
			}

			webpages = append(webpages, Webpage{
				TopAddress: dataCluster.TopAddress,
				Date:       dataCluster.Date,
				Address:    dataCluster.Address,
				Content:    webpage,
				Media:      media,
				Ext:        ext,
			})
		}
	}
	return webpages, clusterFailed
}

func getPage(cluster *communication.DataCluster, client *http.Client, transport *http.Transport) (string, string, uint8, *Proxy) {
	// convert timeout
	timeoutDuration := time.Duration(cluster.Timeout) * time.Second

	client, proxy := updateClient(client, transport, timeoutDuration)

	// create new request
	httpReq, err := http.NewRequest("GET", cluster.Address, nil)
	if err != nil {
		return "", "", communication.ErrPanic, proxy
	}
	httpReq.Close = true

	// make request
	httpRes, err := client.Do(httpReq)
	if err != nil {
		if strings.Contains(err.Error(), "Timeout") {
			return "", "", communication.ErrTimeout, proxy
		} else {
			proxyList.proxyFailed(proxy)
			return "", "", communication.ErrSoft, proxy
		}
	}

	return handleResponse(httpRes, proxy)
}

func handleResponse(res *http.Response, proxy *Proxy) (string, string, uint8, *Proxy) {
	switch res.StatusCode {
	case 200:
		defer res.Body.Close()
		webpage, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return "", "", communication.ErrSoft, proxy
		}
		media := res.Header.Get("Content-Type")
		return string(webpage), media, communication.ErrNone, proxy
	case 404:
		return "", "", communication.ErrPanic, proxy
	default:
		return "", "", communication.ErrSoft, proxy
	}
}

func updateClient(client *http.Client, transport *http.Transport, timeout time.Duration) (*http.Client, *Proxy) {
	selectedProxy := proxyList.selectRandomProxy()
	client.Timeout = timeout

	if selectedProxy.URL == nil {
		return client, selectedProxy
	}

	transport.Proxy = http.ProxyURL(selectedProxy.URL)
	client.Transport = transport

	return client, selectedProxy
}
