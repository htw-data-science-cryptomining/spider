package crawler

import (
	"os"
	"fmt"
	"hash/fnv"
	"strconv"
	"strings"
)

var SAVE_PATH string

func init() {
	SAVE_PATH = os.Getenv("SPIDER_SAVE_PATH")
	if SAVE_PATH == "" {
		panic("Download directory not set, exiting")
	}
}

// save the content on the hard disk
// note if url == "" Store creates the hash of the content
func Store(topAddress string, date uint64, url string, content string, ext string) {
	// create folder
	folderPath := fmt.Sprintf("%s/%s/%d", SAVE_PATH, topAddress, date)
	err := os.MkdirAll(folderPath, 0744)
	if err != nil {
		return
	}

	// create hash
	hash := fnv.New32a()
	if url != "" {
		hash.Write([]byte(url))
	} else {
		hash.Write([]byte(content))
	}

	filePath := fmt.Sprintf("%s/%d%s", folderPath, hash.Sum32(), ext)

	// create file
	file, err := os.Create(filePath)
	if err != nil {
		return
	}
	defer file.Close()

	if ext == ".html" {
		dateString := strconv.FormatUint(date, 10)
		formatedDate := fmt.Sprintf("%s-%s-%s", dateString[0:4], dateString[4:6], dateString[6:8])
		metaTag := "<meta name=\"crawler-date\" content=\""+formatedDate+"\">"
		content = strings.Replace(content, "<head>", "<head>\n"+metaTag, 1)
	}

	// write to file
	_, err = file.WriteString(content)
	if err != nil {
		return
	}

	// sync
	err = file.Sync()
	if err != nil {
		return
	}
}
