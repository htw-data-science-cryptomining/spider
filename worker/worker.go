package worker

import (
	"../communication"
	"sync"
	"./crawler"
	"net/http"
	"fmt"
)

type Worker struct {
	reqChannel chan communication.Request
	resChannel chan communication.Response
	abortChannel chan struct{}
	waitGroup *sync.WaitGroup
}

func NewWorker(reqChannel chan communication.Request, resChannel chan communication.Response, abortChannel chan struct{}, wg *sync.WaitGroup) Worker {
	return Worker{
		reqChannel: reqChannel,
		resChannel: resChannel,
		abortChannel: abortChannel,
		waitGroup: wg,
	}
}

func (w *Worker) Run() {
	defer w.waitGroup.Done()

	client := &http.Client{}
	transport := &http.Transport{}
	transport.DisableKeepAlives = true

	for {
		select {
		case req, cOpen := <-w.reqChannel:
			if !cOpen {
				return
			} else {
				fmt.Printf("New Request:\nTries: %d, Frame: %d, TopAddress: %s\n", req.Try, req.Frame, req.Data[0].TopAddress)
				res := handleRequest(&req, client, transport)
				communication.ResponseChannel <-res
			}
		case <-w.abortChannel:
			return
		}
	}
}

func handleRequest(req *communication.Request, client *http.Client, transport *http.Transport) communication.Response {
	// download cluster array
	webpages, clusterFailed := crawler.DownloadCluster(req.Data, client, transport)

	// iterate over downloaded webpages
	newLinks := make([]communication.DataCluster, 0)
	for _, webpage := range webpages {
		// extract js
		links, embeddedScripts := crawler.ExtractJs(&webpage)
		newLinks = append(newLinks, links...)

		// store webpage
		crawler.Store(webpage.TopAddress, webpage.Date, webpage.Address, webpage.Content, webpage.Ext)

		//store embedded scripts
		for _, embeddedScript := range embeddedScripts {
			crawler.Store(webpage.TopAddress, webpage.Date, "", embeddedScript, ".js")
		}
	}

	return communication.NewResponse(clusterFailed, newLinks, req.Try, req.Frame)
}
