# README #

## Information ##

#### Members ####
Lasse, Daniel

#### Date ####
28/10/2017

#### Status ####
WIP

#### TODO ####
-> new proxies
-> print new request address
-> memory leak fix
-> goroutines only when needed

## Configuration ##
config.json
```
{
	"from": "0",
	"len": "1",
	"all": "false",
	"tries": "1"
}
```

# SIMPLE MAIN -- TEMP #
```
package main


import (
	"fmt"
	"../../../shared/git/bitbucket/datascience/spider/communication"
	"../../../shared/git/bitbucket/datascience/spider/scheduler"
	"../../../shared/git/bitbucket/datascience/spider/workerPool"
	"../../../shared/git/bitbucket/datascience/spider/waybackcsv"
)

func main() {
	communication.Establish()
	fmt.Printf("communication established\n")
	
	sched := scheduler.Sched{}
	sched.ReadCfg("config.json")

	reader := waybackcsv.NewReader("test.csv")
	reader.ReadCfg("config.json")

	pool := workerPool.NewWorkerPool(communication.RequestChannel, communication.ResponseChannel)
	
	sched.Start()
	fmt.Printf("scheduler started (%s)\n", sched.TimeStamp.String())

	pool.ResizeWorkers(8)
	reader.WriteIn(sched.C)

	fmt.Printf("wait for scheduler to complete\n")
	sched.Wait()
	fmt.Printf("scheduling completed\n")
	pool.Close()
	fmt.Printf("pool closed\n")
	fmt.Printf("...\n")
}
```
